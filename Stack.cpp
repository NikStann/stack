
#define __CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#define DEBUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBUG_NEW

#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::endl;

template <class AnyType> //���������� ������ ��� �������� � ���� ������
class MyStack //�������� ������
{
private:
	AnyType* StackArray = NULL;
	int size = 0;

public:

	MyStack()
	{

	}

	~MyStack()
	{

	}
	//��� ������� push. �� ��� ���� "������" ������� push. ���������� ����� ����������.
	void push(AnyType value)
	{
		AnyType* NewArray = new AnyType[size + 1];
		cout << "Add Element " << value << endl;

		for (int i = 0; i < size; i++)
		{
			NewArray[i] = StackArray[i];
		}
		NewArray[size] = value;
		delete[] StackArray;
		size++;
		StackArray = NewArray;
	}

	//��� ������� pop, �������� �������/��������� ����������
	void pop()
	{
		--size;
	}

	//����� ����� ����� � �������
	void Print()
	{
		for (int i = 0; i < size; i++)
		{
			std::cout << *(StackArray + i) << " ";
		}
		std::cout << std::endl;
	}
	//������ �������� �����
	void Del()
	{
		delete[] StackArray;
	}


};
// push - ��������� ������� � � ����
//pop ������� ������� �������
int main()
{
	MyStack<int> IntStack; //���������� ��������� ���������� ��� ������ Int � string ��������������
	MyStack<string> StringStack;

	IntStack.push(1);
	IntStack.push(2);
	IntStack.push(100);
	IntStack.push(5);
	IntStack.push(4);
	IntStack.Print();
	IntStack.pop();
	IntStack.Print();
	IntStack.pop();
	IntStack.Print();
	IntStack.pop();
	IntStack.Print();
	IntStack.push(100);
	IntStack.Print();
	IntStack.Del();

	cout << "----------------" << endl;

	StringStack.push("Test:0");
	StringStack.push("Test:1");
	StringStack.push("Test:2");
	StringStack.push("Test:3");
	StringStack.push("Test:4");
	StringStack.pop();
	StringStack.Print();
	StringStack.pop();
	StringStack.Print();
	StringStack.push("itsWork:1");
	StringStack.Print();
	StringStack.Del();



	_CrtDumpMemoryLeaks(); //�������� ������ ������
	return 0;
}